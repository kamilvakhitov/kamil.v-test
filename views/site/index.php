<?php
	$this->title = Yii::$app->settings->get('SiteSettings', 'homePageTitle') ?: Yii::$app->name;
	if (Yii::$app->settings->get('SiteSettings', 'homePageKeywords')) {
		$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->settings->get('SiteSettings', 'homePageKeywords')]);
	}
	if (Yii::$app->settings->get('SiteSettings', 'homePageDescription')) {
		$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->settings->get('SiteSettings', 'homePageDescription')]);
	}
?>

<?php
	echo \app\widgets\Slider::widget();
	echo \app\widgets\Services::widget();
	echo \app\widgets\Advantages::widget();
?>