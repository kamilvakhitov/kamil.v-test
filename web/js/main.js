(function($) {

  'use strict';

  var waitForFinalEvent = (function () {
    var timers = {};
    return function (callback, ms, uniqueId) {
      if (!uniqueId) {
        uniqueId = "Don't call this twice without a uniqueId";
      }
      if (timers[uniqueId]) {
        clearTimeout (timers[uniqueId]);
      }
      timers[uniqueId] = setTimeout(callback, ms);
    };
  })();

  // Fanycbox gallery
  function fancybox() {
    $('').fancybox({
      selector : '.videos .slick-slide:not(.slick-cloned)',
      loop: true,
      buttons : [
      "zoom",
      "fullScreen",
      "close",
      ],
    });
    $('').fancybox({
      selector : '.project-page .slick-slide:not(.slick-cloned)',
      loop: true,
      buttons : [
      "zoom",
      "fullScreen",
      "close",
      ],
    });
  };

  // Mask
  function mask() {
    $('.phone-mask').mask('+7 (000) 000-00-00', {
        placeholder: "+7 (___) ___-__-__"
    });
  };

  // Tooltip and popover
  function tooltip() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
  };

  // Object fill polyfill
  function objectFill() {
    objectFitImages(null, {watchMQ: true});
  }

  // Table style
  function tableStyle() {
    if ($(".content").length) {
      $(".content").addClass('content-with-tables');
    }
  };

  //Main slider
  function sliderMain() {
    $('.main-slider .slick-slider').each(function() {
      $(this).slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 6000,
        dots: true,
        appendDots: $('.ms-controls .slick-switches .ss-dots', $(this).closest('.main-slider')),
      });

      $('.ms-controls .slick-switches .ss-switch.prev', $(this).closest(".main-slider")).click(function() {
        $('.slick-slider', $(this).closest('.main-slider')).slick('slickPrev');
      });

      $('.ms-controls .slick-switches .ss-switch.next', $(this).closest(".main-slider")).click(function() {
        $('.slick-slider', $(this).closest('.main-slider')).slick('slickNext');
      });

      var numItems =  $('.ms-slide', $(this)).length;
      if (numItems > 1) {
        $('.ms-controls', $(this).closest(".main-slider")).css('display', '');
      }
    })
  };

  // Footer
  function footer() {
    if ($("#bottom").length) {
      var height = Math.round(document.getElementById('bottom').offsetHeight);
      $(".wrapper .content").css('padding-bottom', height);
      $("#bottom").css('margin-top', -height);
    }
  };

  // Footer auto on resize
  function footerAuto() {
    $(window).on('resize', function(){
      waitForFinalEvent(function(){
        footer();
      }, 100, "footer");
    });
  };

  // Scroll to top
  function scrollToTop() {
    if ($('#back-to-top').length) {
      var scrollTrigger = 100,
      backToTop = function () {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > scrollTrigger) {
          $('#back-to-top').addClass('show');
        } else {
          $('#back-to-top').removeClass('show');
        }
      };
      backToTop();
      $(window).on('scroll', function () {
        backToTop();
      });
      $('#back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 400);
      });
    }
  };

  $(document).ready(function() {
    fancybox();
    mask();
    tooltip();
    objectFill();
    tableStyle();
    
    sliderMain();
    footer();
    footerAuto();
    scrollToTop();
  });

})(jQuery);
