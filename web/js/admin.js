(function ($, undefined) {
	$(document).ready(function(){

		/* If mobile */
		if(jQuery.browser.mobile === true) {
			$('body').removeClass('fixed-sidebar');
		}

		/* Hamburger */
		$('.site-header .collapse-button').click(function(){
			if ($('body').hasClass('mobile-menu-opened')) {
				$(this).removeClass('active');
				$('body').removeClass('mobile-menu-opened');
				if(jQuery.browser.mobile === false){
					$('html').css('overflow','auto');
				}
			} else {
				$(this).addClass('active');
				$('body').addClass('mobile-menu-opened');
				if(jQuery.browser.mobile === false){
					$('html').css('overflow','hidden');
				}
			}
		});

		$('.mobile-menu-overlay').click(function(){
			$('.site-header .collapse-button').removeClass('active');
			$('body').removeClass('mobile-menu-opened');
			$('html').css('overflow','auto');
		});

		/* Toggle search */
		$(document).on('click', '.toggle-search', function () {
			if ($('#search-form').hasClass('visible-anyway')) {
				$('.btn-label', this).html('<i class="ti-arrow-down">');
				$('#search-form').removeClass('visible-anyway');
			} else {
				$('.btn-label', this).html('<i class="ti-arrow-up">');
				$('#search-form').addClass('visible-anyway');
			}
		});

//		/* Scroll */
//		if (!('ontouchstart' in document.documentElement)) {
//			var jScrollOptions = {
//				autoReinitialise: true,
//				autoReinitialiseDelay: 100
//			};
//			$('.custom-scroll').jScrollPane(jScrollOptions);
//		}

		/* Sidebar */
		$('.sidebar-menu li.with-sub').each(function(){
			var parent = $(this),
				clickLink = parent.find('>span'),
				subMenu = parent.find('ul');

			clickLink.click(function(){
				if (parent.hasClass('opened')) {
					parent.removeClass('opened');
					subMenu.slideUp();
				} else {
					$('.sidebar-menu li.with-sub').not(this).removeClass('opened').find('ul').slideUp();
					parent.addClass('opened');
					subMenu.slideDown();
				}
			});
		});

		$('.sidebar-menu li.with-sub .active').parents('li:last').children('span:first').trigger('click');
		

		/* Hide notification */
		$(document).on('click', '.menu-editor .notifications .close', function () {
			$(this).parent().hide();
		});
		
		/* Tooltip */
		$("[data-toggle='tooltip']").tooltip();

		/* Selected categories */
//		selectedCategories = function() {
//			if($('#category-grid table tbody tr').length > 0) {
//				$('#category-grid table tbody tr').each(function() {
//					var value = $('input', $(this)).attr('value');
//					var inputParent = $('#add-category-grid table tbody tr').find('input[value=' + value + ']').parent();
//					var button = $('button', inputParent);
//					button.attr('class', 'btn btn-xs btn-success added');
//					$('.glyphicon', button).removeClass('glyphicon-plus').addClass('glyphicon-ok');
//				});
//			}
//		};
//
//		/* Add as related */
//		$(document).on('click', '#add_as_related', function () {
//			var row = $(this).parent().parent();
//        	var value = $('input', $(this).parent()).attr('value');
//
//			if ($(this).hasClass('added')) {
//
//				if($('#related-grid table tbody tr#' + value).length) {
//					$('#related-grid table tbody').find('tr#' + value).remove();
//
//					if($('#related-grid table tbody tr').length == 1 && $('#related-grid table tbody tr td.empty').length) {
//						$('#related-grid table tbody tr td.empty').show();
//					}
//					$(this).removeClass('added');
//					$(this).removeClass('btn-success').addClass('btn-primary');
//					$('.glyphicon', this).removeClass('glyphicon-ok').addClass('glyphicon-plus');
//					$.growl('Удалено!', {
//		                type: 'danger',
//		                z_index: '1060'
//		            });
//				}
//
//			} else {
//
//				if(!$('#related-grid table tbody tr#' + value).length) {
//
//					if($('#related-grid table tbody tr').length == 1 && $('#related-grid table tbody tr td.empty').length) {
//						$('#related-grid table tbody tr td.empty').hide();
//					}
//					var cloned = row.clone().attr({class: 'warning', id: value });
//				    cloned.find('button').remove();
//				    cloned.find('input').prop({type: 'checkbox', checked: 'checked'});
//				    cloned.find('.button-column').removeClass().addClass('checkbox-column');
//					cloned.appendTo($('#related-grid table tbody'));
//
//					$(this).addClass('added');
//		        	$(this).removeClass('btn-primary').addClass('btn-success');
//		        	$('.glyphicon', this).removeClass('glyphicon-plus').addClass('glyphicon-ok');
//		        	$.growl('Привязано!', {
//		                type: 'success',
//		                z_index: '1060'
//		            });
//				}
//
//			}
//		});

		/* Selected relateds */
//		selectedRelateds = function() {
//			if($('#related-grid table tbody tr').length > 0) {
//				$('#related-grid table tbody tr').each(function() {
//					var value = $('input', $(this)).attr('value');
//					var inputParent = $('#product-grid table tbody tr').find('input[value=' + value + ']').parent();
//					var button = $('button', inputParent);
//					button.attr('class', 'btn btn-xs btn-success added');
//					$('.glyphicon', button).removeClass('glyphicon-plus').addClass('glyphicon-ok');
//				});
//			}
//		};

	});
})(jQuery);